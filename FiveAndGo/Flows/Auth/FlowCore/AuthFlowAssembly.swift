//
//  AuthFlowAssembly.swift
//  FiveAndGo
//
//  Created by Eugene on 20.03.2018.
//  Copyright © 2018 Yalantis. All rights reserved.
//

import Swinject
import SwinjectAutoregistration
import APIClient
import Core

final class AuthFlowAssembly: Assembly {
    
    func assemble(container: Container) {
        assembleFlowViewControllers(container: container)
    }
    
    private func assembleFlowViewControllers(container: Container) {
        container.register(LoginViewController.self) { (resolver: Resolver, parent: EventNode) in
            let model = LoginModel(
                parent,
                userSessionController: resolver.autoresolve()
            )
            let controller = StoryboardScene.Authorization.loginViewController.instantiate()
            controller.viewModel = LoginViewModel(model: model)
            return controller
        }.inObjectScope(.transient)
        container.register(IntroViewController.self) { (resolver: Resolver, parent: EventNode) in
            let model = IntroModel(
                parent,
                userSessionController: resolver.autoresolve()
            )
            let controller = StoryboardScene.Authorization.introViewController.instantiate()
            controller.viewModel = IntroViewModel(model: model)
            return controller
        }.inObjectScope(.transient)
        container.register(SignUpViewController.self) { (resolver: Resolver, parent: EventNode) in
            let model = SignUpModel(
                parent,
                userSessionController: resolver.autoresolve()
            )
            let controller = StoryboardScene.Authorization.signUpViewController.instantiate()
            controller.viewModel = SignUpViewModel(model: model)
            return controller
        }.inObjectScope(.transient)
        container.register(SelectSignInViewController.self) { (resolver: Resolver, parent: EventNode) in
            let model = SelectSignInModel(
                parent,
                userSessionController: resolver.autoresolve()
            )
            let controller = StoryboardScene.Authorization.selectSignInViewController.instantiate()
            controller.viewModel = SelectSignInViewModel(model: model)
            return controller
        }.inObjectScope(.transient)
    }
    
}
