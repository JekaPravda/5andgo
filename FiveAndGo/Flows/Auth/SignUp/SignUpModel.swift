//
//  SignUpModel.swift
//  FiveAndGo
//
//  Created by Eugene on 22.03.2018.
//  Copyright (c) 2018 Yalantis. All rights reserved.
//
//

import RxSwift
import Core

enum SignUpEvent: Event {
    
}

final class SignUpModel: EventNode, HasDisposeBag {
    
    // MARK: - Properties.
    
    let requestStateObservable = PublishSubject<RequestState>()
    
    private let userSessionController: UserSessionController
    
    // MARK: - Init.
    
    init(_ parent: EventNode, userSessionController: UserSessionController) {
        self.userSessionController = userSessionController
        
        super.init(parent: parent)
        
        initializeBindings()
    }
    
    // MARK: - Private Method.
    
    private func initializeBindings() {
        
    }
    
}
