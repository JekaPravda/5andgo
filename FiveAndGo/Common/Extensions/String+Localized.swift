//
//  String+Localized.swift
//  Mikitsune
//
//  Created by Konstantin Safronov on 8/4/17.
//  Copyright © 2017 Yalantis. All rights reserved.
//

import Foundation

extension String {
  
  var localized: String {
    return NSLocalizedString(self, comment: self)
  }
  
}
