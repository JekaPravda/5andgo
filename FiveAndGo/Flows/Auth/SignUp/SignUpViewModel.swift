//
//  SignUpViewModel.swift
//  FiveAndGo
//
//  Created by Eugene on 22.03.2018.
//  Copyright (c) 2018 Yalantis. All rights reserved.
//
//

import RxSwift

final class SignUpViewModel: HasDisposeBag {
    
    // MARK: - Properties.
    
    var requestStateObservable: PublishSubject<RequestState> {
        return model.requestStateObservable
    }
    
    private let model: SignUpModel
    
    // MARK: - Init.
    
    init(model: SignUpModel) {
        self.model = model
        
        initializeBindings()
    }
    
    // MARK: - Private Method.
    
    private func initializeBindings() {
        
    }
    
}
