//
//  WebViewController.swift
//  Mikitsune
//
//  Created by Eugene on 16.01.18.
//  Copyright (c) 2018 Yalantis. All rights reserved.
//
//

import UIKit
import RxSwift
import WebKit
import SnapKit

final class WebViewController: UIViewController {
    
    // MARK: - Properties
    
    var viewModel: WebViewModel! {
        didSet {
            bind(requestState: viewModel.requestStateObservable)
        }
    }
    
    @IBOutlet private weak var activityIndicator: UIActivityIndicatorView!
    private let webView = WKWebView()
    
    // MARK: - View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.setNavigationBarHidden(false, animated: false)
        initializeBindings()
        setupView()
    }
    
    // MARK: - Private Methods
    
    private func setupView() {
        view.backgroundColor = .white
        view.addSubview(webView)
        view.sendSubview(toBack: webView)
        webView.navigationDelegate = self
        webView.backgroundColor = .white
        webView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }    }
    
    private func initializeBindings() {
        viewModel.urlForPresentation.asObservable().subscribe(onNext: { [weak self] url in
            self?.webView.load(URLRequest(url: url))
        }).disposed(by: disposeBag)
        
        viewModel.mainTitle.asObservable().subscribe(onNext: { [weak self] mainTitle in
            self?.title = mainTitle
        }).disposed(by: disposeBag)
    }
    
}

extension WebViewController: WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        activityIndicator.stopAnimating()
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        activityIndicator.stopAnimating()
    }
    
}
