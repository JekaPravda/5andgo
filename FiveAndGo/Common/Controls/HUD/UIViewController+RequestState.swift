//
//  UIViewController+RequestState.swift
//
//  Created by Roman Kyrylenko.
//  Copyright © 2018 Yalantis. All rights reserved.
//

import UIKit
import RxSwift

extension UIViewController {
    
    func bind(requestState observable: Observable<RequestState>) {
        observable
            .observeOn(MainScheduler.instance)
            .doOnNext { [weak self] state in
                self?.handle(state)
            }
            .disposed(by: disposeBag)
    }
    
    func handle(_ state: RequestState) {
        switch state {
        case .failed(let error):
            if let error = error {
                show(error: error)
            }
            
        case .errorMessage(let errorMessage):
            if let errorMessage = errorMessage {
                show(error: errorMessage)
            }
            
        case .inProgress(let progress):
            showSpinner(progress: progress)
            
        case .started:
            startSpinnerAnimation()
            
        case .finished:
            stopSpinnerAnimation()
            
        case .success(let message):
            show(success: message)
            
        }
    }
    
}
