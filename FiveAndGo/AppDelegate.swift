//
//  AppDelegate.swift
//  FiveAndGo
//
//  Created by Eugene on 01.02.2018.
//  Copyright © 2018 Yalantis. All rights reserved.
//

import Core
import UIKit
import Fabric
import Crashlytics

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    private var appFlowCoordinator: ApplicationFlowCoordinator?
    private var userSessionController = UserSessionController()
    
    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow(frame: UIScreen.main.bounds)
        
        Fabric.with([Crashlytics.self])
        
        appFlowCoordinator = ApplicationFlowCoordinator(window: window!, userSessionController: userSessionController)
        appFlowCoordinator?.execute()
        return true
    }
    
}
