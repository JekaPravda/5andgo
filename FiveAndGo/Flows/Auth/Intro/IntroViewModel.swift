//
//  IntroViewModel.swift
//  FiveAndGo
//
//  Created by Eugene on 22.03.2018.
//  Copyright (c) 2018 Yalantis. All rights reserved.
//
//

import RxSwift

final class IntroViewModel: HasDisposeBag {
    
    // MARK: - Properties.
    
    var requestStateObservable: PublishSubject<RequestState> {
        return model.requestStateObservable
    }
    
    private let model: IntroModel
    
    // MARK: - Init.
    
    init(model: IntroModel) {
        self.model = model
        
        initializeBindings()
    }
    
    // MARK: - Private Method.
    
    private func initializeBindings() {
        
    }
    
}
