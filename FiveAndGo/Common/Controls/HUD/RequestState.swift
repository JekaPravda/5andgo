//
//  RequestState.swift
//
//  Created by Roman Kyrylenko.
//  Copyright © 2018 Yalantis. All rights reserved.
//

enum RequestState {
    
    case started // start spinner
    case inProgress(Float) // display progress view
    case finished // stop spinner
    case failed(Error?) // display error
    case errorMessage(String?) // display string message
    case success(String?) // display success
    
}
