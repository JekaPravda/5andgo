import Swinject
import SwinjectAutoregistration
import Core

final class ApplicationFlowAssembly: Assembly {
    
    let userSessionController: UserSessionController
    
    init(_ userSessionController: UserSessionController) {
        self.userSessionController = userSessionController
    }
    
    func assemble(container: Container) {
        container.register(
            UserSessionController.self,
            factory: { [unowned userSessionController] _ in return userSessionController }
            ).inObjectScope(.container)
        
        container.register(AuthFlowCoordinator.self) { [unowned container] (resolver, parent: EventNode) in
            return AuthFlowCoordinator(
                container: container,
                parent: parent,
                userSessionController: resolver.autoresolve()
            )
            }.inObjectScope(.transient)
        container.register(MainFlowCoordinator.self) { [unowned container] (_, parent: EventNode) in
            return MainFlowCoordinator(container: container, parent: parent)
            }.inObjectScope(.transient)
        container.register(WebContainerCoordinator.self) { [unowned container] (_, parent: EventNode, linkObject: LinkDTOObject) in
            return WebContainerCoordinator(container: container,
                                           parent: parent,
                                           linkObject: linkObject
            )
            }.inObjectScope(.transient)
    }

}
