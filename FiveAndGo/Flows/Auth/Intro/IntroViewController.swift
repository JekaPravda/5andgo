//
//  IntroViewController.swift
//  FiveAndGo
//
//  Created by Eugene on 22.03.2018.
//  Copyright (c) 2018 Yalantis. All rights reserved.
//
//

import UIKit
import RxSwift

final class IntroViewController: UIViewController {

    // MARK: - Properties
    
    var viewModel: IntroViewModel! {
        didSet {
            bind(requestState: viewModel.requestStateObservable)
        }
    }
    
    // MARK: - IBOutlets
    
    @IBOutlet private weak var skipButton: UIButton!
    @IBOutlet private weak var introPageControl: UIPageControl!
    @IBOutlet private weak var introCollectionView: UICollectionView!
    
    // MARK: - View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initializeBindings()
    }
    
    // MARK: - Private Methods
    
    private func initializeBindings() {

    }
    
}
