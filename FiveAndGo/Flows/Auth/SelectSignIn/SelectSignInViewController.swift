//
//  SelectSignInViewController.swift
//  FiveAndGo
//
//  Created by Eugene on 22.03.2018.
//  Copyright (c) 2018 Yalantis. All rights reserved.
//
//

import UIKit
import RxSwift

final class SelectSignInViewController: UIViewController {
    
    // MARK: - Properties
    
    var viewModel: SelectSignInViewModel! {
        didSet {
            bind(requestState: viewModel.requestStateObservable)
        }
    }
    
    // MARK: - IBOutlets
    
    @IBOutlet private weak var logoImageView: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var skipButton: UIButton!
    @IBOutlet private weak var signInUpButton: UIButton!
    
    // MARK: - View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initializeBindings()
    }
    
    // MARK: - Private Methods
    
    private func initializeBindings() {

    }
    
}
