//
//  NSObject+DisposeBag.swift
//
//  Created by Roman Kyrylenko.
//  Copyright © 2018 Yalantis. All rights reserved.
//

import Foundation
import NSObject_Rx

public typealias HasDisposeBag = NSObject_Rx.HasDisposeBag

extension NSObject: HasDisposeBag {}
