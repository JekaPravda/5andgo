//
//  UIViewController+SpinnerPresentable.swift
//
//  Created by Roman Kyrylenko.
//  Copyright © 2018 Yalantis. All rights reserved.
//

import UIKit
import SVProgressHUD
import RxSwift

extension UIViewController: SpinnerPresentable {
    
    func startSpinnerAnimation(blockUI: Bool) {
        view.endEditing(true)
        SVProgressHUD.setDefaultMaskType(blockUI ? .black : .clear)
        SVProgressHUD.show()
    }
    
    func stopSpinnerAnimation() {
        SVProgressHUD.dismiss()
    }
    
    func showSpinner(progress: Float, blockUI: Bool) {
        view.endEditing(true)
        SVProgressHUD.setDefaultMaskType(blockUI ? .black : .clear)
        SVProgressHUD.showProgress(progress)
    }
    
}

extension UIViewController {
    
    func bindSpinner(observable: Observable<Bool>) {
        observable
            .distinctUntilChanged()
            .observeOn(MainScheduler.instance)
            .doOnNext { [weak self] showAnimation in
                if showAnimation {
                    self?.startSpinnerAnimation()
                } else {
                    self?.stopSpinnerAnimation()
                }
            }
            .disposed(by: disposeBag)
    }
    
}
