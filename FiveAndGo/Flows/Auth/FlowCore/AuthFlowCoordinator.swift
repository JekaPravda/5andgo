//
//  AuthFlowCoordinator.swift
//  ArchitectureGuideTemplate
//
//  Created by Serhii Butenko on 31/10/16.
//  Copyright © 2016 Yalantis. All rights reserved.
//

import UIKit
import Core
import Swinject

enum AuthFlowEvent: Event {
    
    case login(userSession: UserSession)
    
}

final class AuthFlowCoordinator: EventNode, FlowCoordinator {
    
    weak var containerViewController: UIViewController?
    private let userSessionController: UserSessionController
    private let container: Container
    
    init(container: Container, parent: EventNode, userSessionController: UserSessionController) {
        self.userSessionController = userSessionController
        self.container = Container(parent: container) { container in
            return AuthFlowAssembly().assemble(container: container)
        }
        
        super.init(parent: parent)
        
        addHandlers()
    }
    
    func createFlow() -> UIViewController {
        let controller = UIViewController()
        addHandlers()
        return controller
    }
    
    func addHandlers() {
    }

}
