//
//  WebViewModel.swift
//  Mikitsune
//
//  Created by Eugene on 16.01.18.
//  Copyright (c) 2018 Yalantis. All rights reserved.
//
//

import RxSwift

final class WebViewModel: HasDisposeBag {
    
    // MARK: - Properties.
    
    var requestStateObservable: PublishSubject<RequestState> {
        return model.requestStateObservable
    }
    
    
    var urlForPresentation: Observable<URL> {
        return model.linkForPresentation.asObservable().map { link in
            return URL(string: link)!
        }
    }
    
    var mainTitle: Variable<String> {
        return model.linkTitle
    }
    
    private let model: WebModel
    
    // MARK: - Init.
    
    init(model: WebModel) {
        self.model = model
    }
    
}
