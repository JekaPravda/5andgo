//
//  Task+Unwrap.swift
//  DLT Wallet
//
//  Created by Roman Kyrylenko on 2/7/18.
//  Copyright © 2018 Yalantis. All rights reserved.
//

import Foundation
import BoltsSwift
import RxSwift
    
func unwrap<T>(with requestStateObservable: PublishSubject<RequestState>,
               taskProducer: () -> Task<T>,
               mainThread: Bool = false,
               completion: ((T) -> Void)? = nil,
               errorCompletion: ((String) -> Void)? = nil) {
    requestStateObservable.onNext(.started)
    let task = taskProducer()
    func handle(result: Task<T>) {
        if let error = result.error {
            requestStateObservable.onNext(.failed(error))
            errorCompletion?(error.localizedDescription)
            return
        }
        requestStateObservable.onNext(.finished)
        if let result = task.result {
            completion?(result)
        }
    }
    
    if mainThread {
        task.continueWith(.mainThread) { result in
            handle(result: result)
        }
    } else {
        task.continueWith { result in
            handle(result: result)
        }
    }
}
