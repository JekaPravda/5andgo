//
//  SignUpViewController.swift
//  FiveAndGo
//
//  Created by Eugene on 22.03.2018.
//  Copyright (c) 2018 Yalantis. All rights reserved.
//
//

import UIKit
import RxSwift

final class SignUpViewController: UIViewController {
    
    // MARK: - Properties
    
    var viewModel: SignUpViewModel! {
        didSet {
            bind(requestState: viewModel.requestStateObservable)
        }
    }
    
    // MARK: - IBOutlets

    @IBOutlet private weak var submitButton: UIButton!
    @IBOutlet private weak var signUpTextLabel: UILabel!
    @IBOutlet private weak var nameTitleLabel: UILabel!
    @IBOutlet private weak var nameTextField: UITextField!
    @IBOutlet private weak var usernameTitleLabel: UILabel!
    @IBOutlet private weak var usernameTextField: UITextField!
    @IBOutlet private weak var nameErrorLabel: UILabel!
    @IBOutlet private weak var usernameErrorLabel: UILabel!

    // MARK: - View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initializeBindings()
    }
    
    // MARK: - Private Methods
    
    private func initializeBindings() {

    }
    
}
