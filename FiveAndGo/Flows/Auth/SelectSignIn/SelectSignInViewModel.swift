//
//  SelectSignInViewModel.swift
//  FiveAndGo
//
//  Created by Eugene on 22.03.2018.
//  Copyright (c) 2018 Yalantis. All rights reserved.
//
//

import RxSwift

final class SelectSignInViewModel: HasDisposeBag {
    
    // MARK: - Properties.
    
    var requestStateObservable: PublishSubject<RequestState> {
        return model.requestStateObservable
    }
    
    private let model: SelectSignInModel
    
    // MARK: - Init.
    
    init(model: SelectSignInModel) {
        self.model = model
        
        initializeBindings()
    }
    
    // MARK: - Private Method.
    
    private func initializeBindings() {
        
    }
    
}
