//
//  MainFlowCoordinator.swift
//  ArchitectureGuideTemplate
//
//  Created by Artem Havriushov on 10/27/16.
//  Copyright © 2016 Yalantis. All rights reserved.
//

import UIKit
import Core
import Swinject

enum MainFlowEvent: Event {
    
    case logout
    
}

final class MainFlowCoordinator: EventNode, FlowCoordinator {
    
    weak var containerViewController: UIViewController?
    
    private let container: Container
    
    init(container: Container, parent: EventNode) {
        self.container = Container(parent: container) { (container: Container) in
            MainFlowAssembly().assemble(container: container)
        }
        
        super.init(parent: parent)
    }
    
    func createFlow() -> UIViewController {
        let controller = UIViewController()
        addHandlers()
        
        return controller
    }
    
    private func addHandlers() {
        
    }
    
}
