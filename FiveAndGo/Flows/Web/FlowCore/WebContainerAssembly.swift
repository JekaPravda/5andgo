//
//  WebContainerAssembly.swift
//  
//
//  Created by Eugene on 16.01.18.
//

import SwinjectAutoregistration
import Swinject
import RxSwift
import Core

class WebContainerAssembly: Assembly {
    
    func assemble(container: Container) {
        container.register((WebViewController.self)) { (resolver: Resolver, linkObject: LinkDTOObject) in
            let model = WebModel(
                resolver.autoresolve(),
                linkObject: linkObject
            )
            let controller = StoryboardScene.Web.webViewController.instantiate()
            controller.viewModel = WebViewModel(model: model)
            return controller
            }.inObjectScope(.transient)
    }
    
}
