//
//  LoginViewModel.swift
//  FiveAndGo
//
//  Created by Eugene on 20.03.2018.
//  Copyright (c) 2018 Yalantis. All rights reserved.
//
//

import RxSwift

final class LoginViewModel: HasDisposeBag {
    
    // MARK: - Properties.
    
    var requestStateObservable: PublishSubject<RequestState> {
        return model.requestStateObservable
    }
    
    private let model: LoginModel
    
    // MARK: - Init.
    
    init(model: LoginModel) {
        self.model = model
        
        initializeBindings()
    }
    
    // MARK: - Private Method.
    
    private func initializeBindings() {
        
    }
    
}
