//
//  AlertPresentable.swift
//
//  Created by Roman Kyrylenko.
//  Copyright © 2018 Yalantis. All rights reserved.
//

protocol AlertPresentable: class {
  
    func presentAlert(title: String?, message: String?, confirm: (() -> Void)?, decline: (() -> Void)?)
    func show(success: String?, hideAfter: Double, blockUI: Bool, onHide: (() -> Void)?)
    func show(alert: String, hideAfter: Double, blockUI: Bool, onHide: (() -> Void)?)
    func show(error: String, hideAfter: Double, blockUI: Bool, onHide: (() -> Void)?)
    
}

extension AlertPresentable {
    
    func show(success: String?) {
        show(success: success, hideAfter: 1, blockUI: true, onHide: nil)
    }
    
    func show(alert: String) {
        show(alert: alert, hideAfter: 1, blockUI: true, onHide: nil)
    }
    
    func show(error: String) {
        show(error: error, hideAfter: 1, blockUI: true, onHide: nil)
    }
    
    func show(error: Error) {
        show(error: error, hideAfter: 1, blockUI: true, onHide: nil)
    }
    
    func show(error: Error, hideAfter: Double, blockUI: Bool, onHide: (() -> Void)?) {
        show(error: error.localizedDescription, hideAfter: hideAfter, blockUI: blockUI, onHide: onHide)
    }
    
}
