//
//  WebModel.swift
//  Mikitsune
//
//  Created by Eugene on 16.01.18.
//  Copyright (c) 2018 Yalantis. All rights reserved.
//
//

import RxSwift
import Core

enum WebEvent: Event {
    
}

struct LinkDTOObject {
    
    let link: String
    let title: String
    
}

final class WebModel: EventNode, HasDisposeBag {
    
    // MARK: - Properties.
    
    let requestStateObservable = PublishSubject<RequestState>()
    let linkForPresentation = Variable("")
    let linkTitle = Variable("")
    
    // MARK: - Init.
    
    init(_ parent: EventNode, linkObject: LinkDTOObject) {
        super.init(parent: parent)
        
        linkForPresentation.value = linkObject.link
        linkTitle.value = linkObject.title
    }
    
}
