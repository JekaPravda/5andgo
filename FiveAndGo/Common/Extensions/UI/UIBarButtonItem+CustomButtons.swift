//
//  UIBarButtonItem+CustomButtons.swift
//  ArchitectureGuideTemplate
//
//  Created by Artem Havriushov on 10/19/16.
//  Copyright © 2016 Yalantis. All rights reserved.
//

import UIKit

extension UIBarButtonItem {
    
    convenience init(
        image: UIImage,
        highlightedImage: UIImage? = nil,
        imageScale: CGFloat = 1,
        target: AnyObject,
        selector: Selector
        ) {
        let button = UIButton(type: .custom)
        button.setImage(image, for: .normal)
        button.setImage(highlightedImage, for: .highlighted)
        button.addTarget(target, action: selector, for: .touchUpInside)
        button.frame = CGRect(
            x: 0,
            y: 0,
            width: image.size.width * imageScale,
            height: image.size.height * imageScale
        )
        
        self.init(customView: button)
    }
    
    convenience init(title: String,
                     target: AnyObject,
                     selector: Selector,
                     attributes: [NSAttributedStringKey: Any]? = nil) {
        self.init(
            title: title,
            style: .plain,
            target: target,
            action: selector
        )
        
        setTitleTextAttributes(attributes, for: .normal)
    }
    
}
