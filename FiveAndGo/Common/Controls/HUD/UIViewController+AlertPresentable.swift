//
//  UIViewController+AlertPresentable.swift
//
//  Created by Roman Kyrylenko.
//  Copyright © 2018 Yalantis. All rights reserved.
//

import UIKit
import SVProgressHUD
import RxSwift

extension UIViewController: AlertPresentable {
    
    func presentAlert(title: String?,
                      message: String?,
                      confirm: (() -> Void)?,
                      decline: (() -> Void)?) {
        let alertViewController = UIAlertController(
            title: title,
            message: message,
            preferredStyle: .alert
        )
        let cancelAction = UIAlertAction(
        title: "cancel".localized,
        style: .cancel) { _ in
            decline?()
        }
        let yesAction = UIAlertAction(
        title: "yes".localized,
        style: .destructive) { _ in
            confirm?()
        }
        
        alertViewController.addAction(yesAction)
        alertViewController.addAction(cancelAction)
        present(alertViewController, animated: true, completion: nil)
    }
    
    func show(error: String, hideAfter: Double, blockUI: Bool, onHide: (() -> Void)?) {
        view.endEditing(true)
        SVProgressHUD.setDefaultMaskType(blockUI ? .black : .clear)
        SVProgressHUD.showError(withStatus: error)
        SVProgressHUD.dismiss(withDelay: hideAfter) {
            DispatchQueue.main.async {
                onHide?()
            }
        }
    }
    
    func show(alert: String, hideAfter: Double, blockUI: Bool, onHide: (() -> Void)?) {
        view.endEditing(true)
        SVProgressHUD.setDefaultMaskType(blockUI ? .black : .clear)
        SVProgressHUD.showInfo(withStatus: alert)
        SVProgressHUD.dismiss(withDelay: hideAfter) {
            DispatchQueue.main.async {
                onHide?()
            }
        }
    }

    func show(success: String?, hideAfter: Double, blockUI: Bool, onHide: (() -> Void)?) {
        view.endEditing(true)
        SVProgressHUD.setDefaultMaskType(blockUI ? .black : .clear)
        SVProgressHUD.showSuccess(withStatus: success)
        SVProgressHUD.dismiss(withDelay: hideAfter) {
            DispatchQueue.main.async {
                onHide?()
            }
        }
    }
    
}

extension UIViewController {
    
    func bindError(observable: Observable<Error?>) {
        observable
            .unwrap()
            .observeOn(MainScheduler.instance)
            .doOnNext { [weak self] error in
                self?.show(error: error)
            }
            .disposed(by: disposeBag)
    }
    
    func bindMessage(observable: Observable<String>) {
        observable
            .distinctUntilChanged()
            .observeOn(MainScheduler.instance)
            .doOnNext { [weak self] message in
                self?.show(alert: message)
            }
            .disposed(by: disposeBag)
    }
    
    func bindSuccess(observable: Observable<Bool>, message: String) {
        observable
            .observeOn(MainScheduler.instance)
            .doOnNext { [weak self] result in
                if result {
                    self?.show(alert: message)
                }
            }
            .disposed(by: disposeBag)
    }
    
}
