//
//  SpinnerPresentable.swift
//
//  Created by Roman Kyrylenko.
//  Copyright © 2018 Yalantis. All rights reserved.
//

protocol SpinnerPresentable: class {
    
    func startSpinnerAnimation(blockUI: Bool)
    func stopSpinnerAnimation()
    func showSpinner(progress: Float, blockUI: Bool)
    
}

extension SpinnerPresentable {
    
    func startSpinnerAnimation() {
        startSpinnerAnimation(blockUI: true)
    }
    
    func showSpinner(progress: Float) {
        showSpinner(progress: progress, blockUI: true)
    }
    
}
