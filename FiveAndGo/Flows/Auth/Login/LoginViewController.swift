//
//  LoginViewController.swift
//  FiveAndGo
//
//  Created by Eugene on 20.03.2018.
//  Copyright (c) 2018 Yalantis. All rights reserved.
//
//

import UIKit
import RxSwift

final class LoginViewController: UIViewController {
    
    // MARK: - Properties
    
    var viewModel: LoginViewModel! {
        didSet {
            bind(requestState: viewModel.requestStateObservable)
        }
    }
    
    // MARK: - IBOutlets
    
    @IBOutlet private weak var submitButton: UIButton!
    @IBOutlet private weak var emailTitleLabel: UILabel!
    @IBOutlet private weak var emailTextField: UITextField!
    @IBOutlet private weak var checkBoxButton: UIButton!
    @IBOutlet private weak var termsStartLabel: UILabel!
    @IBOutlet private weak var termsButton: UIButton!
    @IBOutlet private weak var loginTextLabel: UILabel!
    @IBOutlet private weak var emailErrorLabel: UILabel!
    
    // MARK: - View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initializeBindings()
    }
    
    // MARK: - Private Methods
    
    private func initializeBindings() {

    }
    
}
