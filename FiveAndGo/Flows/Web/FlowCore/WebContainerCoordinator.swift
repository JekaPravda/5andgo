//
//  WebContainerCoordinator.swift
//  Mikitsune
//
//  Created by Eugene on 16.01.18.
//  Copyright © 2018 Yalantis. All rights reserved.
//

import UIKit
import Swinject
import SwinjectAutoregistration
import Core
import RxSwift

final class WebContainerCoordinator: EventNode {
  
  private var container: Container!
  private weak var root: UIViewController!
  private let linkObject: LinkDTOObject

    init(container: Container, parent: EventNode, linkObject: LinkDTOObject) {
    self.linkObject = linkObject
    super.init(parent: parent)
    
    registerFlow()
  }
  
  private func registerFlow() {
    self.container = Container(parent: container) { container in
        return WebContainerAssembly().assemble(container: container)
    }
  }
    
  func createFlow() -> UIViewController {
    let vc = container.resolve(WebViewController.self, argument: linkObject)!
    vc.title = linkObject.title
    root = vc
    return root
  }

}
